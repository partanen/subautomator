#!/bin/bash

# Private helper functions

Process_file() {
  filename=$(basename -- "$1") # filu.asd
  extension="${filename##*.}"  # asd

  supported=("srt" "sub" "mkv")
  if [[ "${supported[*]}" =~ ${extension} ]]; then
    $subeditpath /convert "$1" SubRip /removetextforhi /fixcommonerrors /overwrite \
     | tail -q -n +4 | grep -v -e '^$' # print last 4 lines of subedit output without empty lines
  fi
}

Process_input_argument() {
  if [ -r "$1" ]; then
    if [ -d "$1" ]; then
      # Directory
      for f in "$1"/*; do
        [[ -e "$f" ]] || break # handle the case of no files
        Process_input_argument "$f"
      done

    elif [ -r "$1" ]; then
      # File
      Process_file "$1"
    else
      echo "Could not read $1"
    fi
  fi
}

# Begin

subexe="SubtitleEdit.exe"
subeditpath=$(locate "$subexe")
if [ ! -x "$subeditpath" ]; then
  echo "Could not locate $subexe"
  exit 1
fi

# Reading argument values in loop
for argval in "$@"; do
  Process_input_argument "$argval"
done
